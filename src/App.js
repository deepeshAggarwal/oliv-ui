import React, { Component } from 'react';
import './App.css';
import LeftBar from './components/LeftBar/index';
import PlaceListView from './components/PlaceListView/index';
import PlaceMapView from './components/PlaceMapView/index';
import TopNavBar from './components/TopNavBar/index';
import { Row, Col } from 'reactstrap';
const request = require('superagent');

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      'results': [
        {
            "id": "c4EhUK4Ibvv5oEjGuUEHSQ",
            "alias": "a-sack-of-potatoes-fremont-2",
            "name": "A Sack of Potatoes",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/tOTyqeDKc8l516hioKXnUA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/a-sack-of-potatoes-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 48,
            "categories": [
                {
                    "alias": "bubbletea",
                    "title": "Bubble Tea"
                },
                {
                    "alias": "hotdogs",
                    "title": "Fast Food"
                },
                {
                    "alias": "pastashops",
                    "title": "Pasta Shops"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5603344028298,
                "longitude": -122.010552883005
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "37100 Fremont Blvd",
                "address2": "Ste C",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94536",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "37100 Fremont Blvd",
                    "Ste C",
                    "Fremont, CA 94536"
                ]
            },
            "phone": "+15108968070",
            "display_phone": "(510) 896-8070",
            "distance": 3491.066226073898
        },
        {
            "id": "Ypw7qe_L9xAxsMKOb1DO_Q",
            "alias": "little-by-little-fremont-2",
            "name": "Little by Little",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/2JI4L-ok5UNLKluYf6rrxw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/little-by-little-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 202,
            "categories": [
                {
                    "alias": "cocktailbars",
                    "title": "Cocktail Bars"
                },
                {
                    "alias": "newamerican",
                    "title": "American (New)"
                },
                {
                    "alias": "gastropubs",
                    "title": "Gastropubs"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 37.493802,
                "longitude": -121.929835
            },
            "transactions": [
                "restaurant_reservation"
            ],
            "price": "$$",
            "location": {
                "address1": "46164 Warm Springs Blvd",
                "address2": "Ste 274",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94539",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "46164 Warm Springs Blvd",
                    "Ste 274",
                    "Fremont, CA 94539"
                ]
            },
            "phone": "+15108976678",
            "display_phone": "(510) 897-6678",
            "distance": 6813.106118179459
        },
        {
            "id": "xqNxkRGOPxCDA0QnEVcVAg",
            "alias": "ka-yumi-diner-fremont",
            "name": "KA YuMi Diner",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/8fNbCBVH_ApNeYf6xTSvdQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/ka-yumi-diner-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 479,
            "categories": [
                {
                    "alias": "korean",
                    "title": "Korean"
                },
                {
                    "alias": "asianfusion",
                    "title": "Asian Fusion"
                },
                {
                    "alias": "chicken_wings",
                    "title": "Chicken Wings"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.534781,
                "longitude": -121.96496
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "40645 Fremont Blvd",
                "address2": "Ste 8",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "40645 Fremont Blvd",
                    "Ste 8",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15103531088",
            "display_phone": "(510) 353-1088",
            "distance": 1917.225744065256
        },
        {
            "id": "wc493deNyzxJ15ohGuNspA",
            "alias": "vons-chicken-fremont",
            "name": "Vons Chicken",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/5FQhGIsEBdHgFGoZrSMDow/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/vons-chicken-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 56,
            "categories": [
                {
                    "alias": "korean",
                    "title": "Korean"
                },
                {
                    "alias": "chickenshop",
                    "title": "Chicken Shop"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5355733234144,
                "longitude": -121.998047903233
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "5010 Mowry Ave",
                "address2": "",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "5010 Mowry Ave",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15104940900",
            "display_phone": "(510) 494-0900",
            "distance": 1001.6426281525474
        },
        {
            "id": "DZR_JRTShGdXpdI3hv983w",
            "alias": "skilletz-cafe-fremont",
            "name": "Skillet'z Cafe",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/KfvQdJhZRNkOCnwxuoT33w/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/skilletz-cafe-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 934,
            "categories": [
                {
                    "alias": "newamerican",
                    "title": "American (New)"
                },
                {
                    "alias": "breakfast_brunch",
                    "title": "Breakfast & Brunch"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 37.57747,
                "longitude": -121.98032
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "37378 Niles Blvd",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94536",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "37378 Niles Blvd",
                    "Fremont, CA 94536"
                ]
            },
            "phone": "+15107938161",
            "display_phone": "(510) 793-8161",
            "distance": 4722.910835035904
        },
        {
            "id": "p5uqf2ujdisFkjkMDAT9Ow",
            "alias": "sweet-garden-fremont",
            "name": "Sweet Garden",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/n8y8BTb8O40zaLvAEGLgCQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/sweet-garden-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 220,
            "categories": [
                {
                    "alias": "asianfusion",
                    "title": "Asian Fusion"
                },
                {
                    "alias": "sushi",
                    "title": "Sushi Bars"
                },
                {
                    "alias": "ramen",
                    "title": "Ramen"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 37.5438591450212,
                "longitude": -121.981836226954
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$",
            "location": {
                "address1": "39473 Fremont Blvd",
                "address2": "",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "39473 Fremont Blvd",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15107719518",
            "display_phone": "(510) 771-9518",
            "distance": 1046.0377098819436
        },
        {
            "id": "vIC6NMct6v9a3k2XAgNUJA",
            "alias": "dish-n-dash-fremont-2",
            "name": "Dish n' Dash",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/IuniNHoJj6FbEK8xUwpPwQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/dish-n-dash-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 579,
            "categories": [
                {
                    "alias": "mideastern",
                    "title": "Middle Eastern"
                },
                {
                    "alias": "juicebars",
                    "title": "Juice Bars & Smoothies"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 37.5048876,
                "longitude": -121.9701053
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "43514 Christy St",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "43514 Christy St",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15102499220",
            "display_phone": "(510) 249-9220",
            "distance": 3647.457194148029
        },
        {
            "id": "i_GuYJ_1hmW-lHeQ4GmOiw",
            "alias": "lazy-dog-restaurant-and-bar-newark-2",
            "name": "Lazy Dog Restaurant & Bar",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/K16XP58xbk-Vf39WBgawvg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/lazy-dog-restaurant-and-bar-newark-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 402,
            "categories": [
                {
                    "alias": "newamerican",
                    "title": "American (New)"
                },
                {
                    "alias": "burgers",
                    "title": "Burgers"
                },
                {
                    "alias": "sandwiches",
                    "title": "Sandwiches"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.52594,
                "longitude": -122.00612
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "3100 Newpark Mall",
                "address2": null,
                "address3": "",
                "city": "Newark",
                "zip_code": "94560",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "3100 Newpark Mall",
                    "Newark, CA 94560"
                ]
            },
            "phone": "+15109252424",
            "display_phone": "(510) 925-2424",
            "distance": 2024.584295925018
        },
        {
            "id": "yKpUdq-5cP5KJRq_MOw86Q",
            "alias": "din-ding-dumpling-house-fremont",
            "name": "Din Ding Dumpling House",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/ejzX_aYIOlJo4pB8iNdXcg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/din-ding-dumpling-house-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 1054,
            "categories": [
                {
                    "alias": "shanghainese",
                    "title": "Shanghainese"
                }
            ],
            "rating": 3.5,
            "coordinates": {
                "latitude": 37.550794,
                "longitude": -121.980952
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "3213 Walnut Ave",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "3213 Walnut Ave",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15107978122",
            "display_phone": "(510) 797-8122",
            "distance": 1798.1792315438197
        },
        {
            "id": "oa0k4i-zi_TdE-DQvCJNmg",
            "alias": "bings-dumpling-fremont",
            "name": "Bing's Dumpling",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/y-ts-X1Ma3AufHb-mPN92A/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/bings-dumpling-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 552,
            "categories": [
                {
                    "alias": "dimsum",
                    "title": "Dim Sum"
                },
                {
                    "alias": "noodles",
                    "title": "Noodles"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5752032935909,
                "longitude": -122.040460221469
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "34360 Fremont Blvd",
                "address2": "",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94555",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "34360 Fremont Blvd",
                    "Fremont, CA 94555"
                ]
            },
            "phone": "+15103205360",
            "display_phone": "(510) 320-5360",
            "distance": 6494.113036125059
        },
        {
            "id": "sDg03monU9sFPeFQ-pNOVA",
            "alias": "king-of-thai-noodle-fremont",
            "name": "King of Thai Noodle",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/ikogyobBD3dx57m3Qk6ZBg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/king-of-thai-noodle-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 226,
            "categories": [
                {
                    "alias": "thai",
                    "title": "Thai"
                }
            ],
            "rating": 3.5,
            "coordinates": {
                "latitude": 37.5526555,
                "longitude": -122.0544207
            },
            "transactions": [
                "restaurant_reservation",
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "34787 Ardenwood Blvd",
                "address2": "",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94555",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "34787 Ardenwood Blvd",
                    "Fremont, CA 94555"
                ]
            },
            "phone": "+15105651767",
            "display_phone": "(510) 565-1767",
            "distance": 6301.250419357557
        },
        {
            "id": "bbHIorPNPczGO2cGDruSQg",
            "alias": "the-halal-guys-fremont-4",
            "name": "The Halal Guys",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/FspDzttA0raTtOhwtwpWUQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/the-halal-guys-fremont-4?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 120,
            "categories": [
                {
                    "alias": "halal",
                    "title": "Halal"
                },
                {
                    "alias": "falafel",
                    "title": "Falafel"
                }
            ],
            "rating": 3.5,
            "coordinates": {
                "latitude": 37.50291,
                "longitude": -121.96977
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "5338 Curie St",
                "address2": null,
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "5338 Curie St",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15104937330",
            "display_phone": "(510) 493-7330",
            "distance": 3919.7033293448912
        },
        {
            "id": "s_f_ZsFqWa1VJAAc1pXiqw",
            "alias": "sala-thai-fremont-2",
            "name": "Sala Thai",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/B4s08N92VGjqcOvNRXWl0Q/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/sala-thai-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 2103,
            "categories": [
                {
                    "alias": "thai",
                    "title": "Thai"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5505241,
                "longitude": -121.9793462
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "3241 Walnut Ave",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "3241 Walnut Ave",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15107920770",
            "display_phone": "(510) 792-0770",
            "distance": 1814.9885549497135
        },
        {
            "id": "8wgPmxKcX70pwvO0zKlfew",
            "alias": "yokohama-iekei-ramen-fremont-2",
            "name": "Yokohama Iekei Ramen",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/d6sUnvaNmJ4fK33aMyUafA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/yokohama-iekei-ramen-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 35,
            "categories": [
                {
                    "alias": "ramen",
                    "title": "Ramen"
                },
                {
                    "alias": "noodles",
                    "title": "Noodles"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.54775,
                "longitude": -121.98474
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "39206 Fremont Blvd",
                "address2": "",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "39206 Fremont Blvd",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15106757433",
            "display_phone": "(510) 675-7433",
            "distance": 1391.0869459174141
        },
        {
            "id": "H8GLq1c-VXz6Krf_tWqqJw",
            "alias": "country-way-fremont-2",
            "name": "Country Way",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/FxzS62OfhqG_F-7Et5CYkw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/country-way-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 2090,
            "categories": [
                {
                    "alias": "tradamerican",
                    "title": "American (Traditional)"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5330543518066,
                "longitude": -122.001739501953
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "5325 Mowry Ave",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "5325 Mowry Ave",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15107973188",
            "display_phone": "(510) 797-3188",
            "distance": 1338.5745451956905
        },
        {
            "id": "JPItDtK9nz1oMW2vt6XU3g",
            "alias": "papillon-fremont",
            "name": "Papillon",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/w58LhBdf10abicBHlA41FQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/papillon-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 776,
            "categories": [
                {
                    "alias": "french",
                    "title": "French"
                },
                {
                    "alias": "bars",
                    "title": "Bars"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.57926,
                "longitude": -121.979734
            },
            "transactions": [
                "restaurant_reservation",
                "pickup",
                "delivery"
            ],
            "price": "$$$",
            "location": {
                "address1": "37296 Mission Blvd",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94536",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "37296 Mission Blvd",
                    "Fremont, CA 94536"
                ]
            },
            "phone": "+15107936331",
            "display_phone": "(510) 793-6331",
            "distance": 4929.042701463574
        },
        {
            "id": "WhjrtEC7Osv5Uv-EXIexxQ",
            "alias": "gen-korean-bbq-house-fremont-3",
            "name": "Gen Korean BBQ House",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/dFXPj0Yo6tX5ASrLiKL9sg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/gen-korean-bbq-house-fremont-3?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 1578,
            "categories": [
                {
                    "alias": "korean",
                    "title": "Korean"
                },
                {
                    "alias": "bbq",
                    "title": "Barbeque"
                },
                {
                    "alias": "asianfusion",
                    "title": "Asian Fusion"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.5033561831859,
                "longitude": -121.975302103375
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "43476 Boscell Rd",
                "address2": "Ste M6-B",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "43476 Boscell Rd",
                    "Ste M6-B",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15105730447",
            "display_phone": "(510) 573-0447",
            "distance": 3688.8657503254417
        },
        {
            "id": "6yV_yKXWTEgmBbg27T9-Xg",
            "alias": "bun-appétit-donuts-fremont-2",
            "name": "Bun Appétit Donuts",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/nfp5cS83VWveAq40UeT3ng/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/bun-app%C3%A9tit-donuts-fremont-2?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 265,
            "categories": [
                {
                    "alias": "donuts",
                    "title": "Donuts"
                },
                {
                    "alias": "breakfast_brunch",
                    "title": "Breakfast & Brunch"
                },
                {
                    "alias": "coffee",
                    "title": "Coffee & Tea"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 37.559903868343,
                "longitude": -122.009873324027
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "37120 Fremont Blvd",
                "address2": "Ste A",
                "address3": null,
                "city": "Fremont",
                "zip_code": "94536",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "37120 Fremont Blvd",
                    "Ste A",
                    "Fremont, CA 94536"
                ]
            },
            "phone": "+15105651292",
            "display_phone": "(510) 565-1292",
            "distance": 3416.818035315806
        },
        {
            "id": "yjr9IpJgGGiV8jViAXeKkQ",
            "alias": "mingala-restaurant-fremont-3",
            "name": "Mingala Restaurant",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/UnnXyU5HyKfmNc1szsJ2xA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/mingala-restaurant-fremont-3?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 228,
            "categories": [
                {
                    "alias": "burmese",
                    "title": "Burmese"
                },
                {
                    "alias": "malaysian",
                    "title": "Malaysian"
                },
                {
                    "alias": "seafood",
                    "title": "Seafood"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.48729,
                "longitude": -121.92852
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "46999 Warm Springs Blvd",
                "address2": null,
                "address3": "",
                "city": "Fremont",
                "zip_code": "94539",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "46999 Warm Springs Blvd",
                    "Fremont, CA 94539"
                ]
            },
            "phone": "+15102521828",
            "display_phone": "(510) 252-1828",
            "distance": 7395.2323653347075
        },
        {
            "id": "Qu-U2jL_cEY_8SeQwyQHZA",
            "alias": "bob-sang-korean-bbq-and-tofu-fremont",
            "name": "Bob Sang Korean BBQ & Tofu",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/JaHzeZA-y4_9oGI5UVzMqA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/bob-sang-korean-bbq-and-tofu-fremont?adjust_creative=0NfsPkbvLc56Xfu2Nc_gwg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=0NfsPkbvLc56Xfu2Nc_gwg",
            "review_count": 1227,
            "categories": [
                {
                    "alias": "korean",
                    "title": "Korean"
                },
                {
                    "alias": "bbq",
                    "title": "Barbeque"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 37.4910141445603,
                "longitude": -121.950362822089
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "4185 Cushing Pkwy",
                "address2": "",
                "address3": "",
                "city": "Fremont",
                "zip_code": "94538",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "4185 Cushing Pkwy",
                    "Fremont, CA 94538"
                ]
            },
            "phone": "+15102529844",
            "display_phone": "(510) 252-9844",
            "distance": 5873.037255745792
        }],
      'center': {
        lat: 37.5603344028298, 
        lng: -122.010552883005
      },
      mapView: false,
      listView: true
    }
  }

  componentDidMount = () => {
    window.addEventListener("resize", this.updateDimensions); 
  }

  onUpdateSearch = (search, type) => {
    let query = '?term=restaurants';
    if(type === 'LOCATION') {
      query += `&latitude=${search[0]}&longitude=${search[1]}`
    } else {
      query += `&location=${search.description}`
    }
    let url = 'http://localhost:3001/search' + query;
    request
      .get(url)
      .set('Authorization', 'Bearer RfcYaddEVhfsFhyy9ZQWNM6LoSflk1mUydj6uZ3_ADJr6mapmI5vTHvz3aZxqz44vAX5jxKS8qY_woljd84J36J0FINhHlgOidD51NQPrwiLHWjNfGs8BySElIKVXHYx')
      .then((res) => {
        let body = res.body;
        this.setState({
          results: body.businesses,
          center: {
            lat: body.region.center.latitude,
            lng: body.region.center.longitude
          }
        })
      });
  }

  toggleView = () => {
    this.setState((prevState) => ({
      mapView: !prevState.mapView,
      listView: !prevState.listView
    }));
  }

  updateDimensions = () => {
    let body = document.getElementsByTagName('body')[0];
    let width = window.innerWidth || document.documentElement.clientWidth || body.clientWidth;
    console.log(width);
    if(width > '767.98') {
      this.setState((prevState) => ({
      mapView: false,
      listView: true
    }));   
    }
  }

  render() {
    return (
      <Col style={{'padding':'0'}} id='mainContainer'>
        <Row id='mainRow'>
          <Col md='4' xs='12' className={ this.state.listView ? 'height-100 padding-0 ' : 'height-200 padding-0'} id='left-bar'>
            <TopNavBar onUpdateSearch={this.onUpdateSearch} toggleView={this.toggleView} mapView={this.state.mapView} style={{height:'100px'}}/>
            <PlaceListView className={ this.state.listView ? 'overflow-auto height-100' : 'd-none'} results={this.state.results} />
          </Col>
          <Col md='8' xs='12' className={ !this.state.mapView ? 'd-sm-none d-md-block' : 'd-md-block'} style={{'padding': 0}} id='right-bar'>
            <PlaceMapView onUpdateSearch={this.onUpdateSearch} results={this.state.results} center={this.state.center} />
          </Col>
        </Row>
      </Col>
    );
  }
}

export default App;
