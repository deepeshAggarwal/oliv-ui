import React, {Component} from 'react';
import { Container, ListGroup, ListGroupItem } from 'reactstrap';
import ItemView from './ItemView';
class PlaceListView extends Component {
  render() {
    return (
      <Container fluid={true} className={this.props.className}>
	      <ListGroup flush>
	        {
	        	this.props.results.length > 0 ?
	        		this.props.results.map((value, index) => {
		        		return (
		        			<ListGroupItem key={index}>
		        				<ItemView value={value} />
		        			</ListGroupItem>
		        		)
							}) : null
	        }
	      </ListGroup>
      </Container>
    );
  }
}

export default PlaceListView;