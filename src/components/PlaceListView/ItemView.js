import React, {Component} from 'react';
import { Row, Col } from 'reactstrap';

class ItemView extends Component {
  render() {
  	let item = this.props.value;
    return (
    	<a href={item.url} target='_blank' rel='noopener noreferrer'>
	      <Row style={{padding:0}}>
	      	<Col xs='9'>
						<div className='title'>{item.name}</div>
						<div className='details'>
						</div>
					</Col>
					<Col xs='3'>
						<img 
							src={item.image_url} 
							alt='Restaurant'
							style={{
								height:'100px',
								width:'100%'
							}}
						/>
					</Col>
	      </Row>
      </a>	
    );
  }
}

export default ItemView;