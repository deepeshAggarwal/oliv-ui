import React, {Component} from 'react';
import { Row } from 'reactstrap';

class LeftBar extends Component {

  render() {
    return (
      <Row id={this.props.id}>
        {this.props.children}
      </Row>
    );
  }
}

export default LeftBar;