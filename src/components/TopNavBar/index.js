import React, {Component} from 'react';
import { Container, Col, Row } from 'reactstrap';
import { Button } from 'reactstrap';
import SearchBox from './SearchBox';
class TopNavBar extends Component {

  useUserLocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      this.props.onUpdateSearch([position.coords.latitude, position.coords.longitude], 'LOCATION')
    });
  }

  render() {
    return (
    	<Container fluid={true} style={{'backgroundColor': '#4285F4', height: '200px'}}>
        <SearchBox updateSearch={this.props.onUpdateSearch} />
        <Container fluid={true} style={{marginTop: '10px'}}>
          <Row className='justify-content-between'>
  	      	<Col xs='5' md='12'>
  	      		<Button id='use-localation' color="primary" onClick={this.useUserLocation}>USE MY LOCATION</Button>
  	      	</Col>
  	      	<Col xs='5' className='d-sm-block d-md-none'>
  	      		<Button onClick={this.props.toggleView} color="primary" style={{float:'right'}}>
                {
                  this.props.mapView ? 'Show List' : 'Show On Map'
                }
              </Button>
  	      	</Col>
          </Row>
        </Container>  
      </Container>
    );
  }
}

export default TopNavBar;