import React, {Component} from 'react';
import { Container } from 'reactstrap';
import { InputGroup, Input, ListGroup, ListGroupItem } from 'reactstrap';
import scriptLoader from 'react-async-script-loader'
class SearchBox extends Component {

	constructor(props) {
		super(props);
		this.state = {
			value: '', 
			results: [],
		};
		this.services = null;
	}

	componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
      	this.service = new window.google.maps.places.AutocompleteService();
      } else {
      	this.props.onError();
      }
    }
  }

	onChange = (event) => {
		this.setState({
  		value: event.target.value
  	}, () => {
  		if(this.state.value) { 
  			this.service.getPlacePredictions({ input: this.state.value }, this.updateResult);	
  		} else {
  			this.setState({
  				results: []
  			})
  		}
  	})
		
	}

	updateResult = (results) => {
		console.log(results);
		this.setState({
			results: results
		});
	}

	updateSearch = (index) => {
    this.setState({
      value: this.state.results[index].description,
      results: []
    });
    // update back to 
    this.props.updateSearch(this.state.results[index]);
  }

  render() {
    return (
      <Container fluid={true} id='search-box'>
      	<InputGroup>
        	<Input id='search-location' placeholder="Search Restaurant"
        	value={this.state.value}
        	onChange={this.onChange}
        	 />
       	</InputGroup>
       	{
       		this.state.results.length > 0 ? (
       			<ListGroup flush>
       			{
		        	this.state.results.map((value, index) => {
		        		return <ListGroupItem key={index} onClick={() =>{this.updateSearch(index)}}>{value.description}</ListGroupItem>
      				})
		        }
			      </ListGroup>
       		) : null
       	}
      </Container>
    );
  }
}
export default scriptLoader(
    ['https://maps.googleapis.com/maps/api/js?key=AIzaSyDAQOhuvUriLPgDzVblnSSH7BUj-s2EMSw&libraries=places']
)(SearchBox);