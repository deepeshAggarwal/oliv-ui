import React, {Component} from 'react';
import scriptLoader from 'react-async-script-loader'

class PlaceMapView extends Component {

  constructor(props) {
    super(props);
    this.markers = [];
    this.map = null;
  }

	componentWillReceiveProps (nextProps) {
    let { isScriptLoaded, isScriptLoadSucceed, results, center } = nextProps;
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        let mapCenter = {lat: -33.8688, lng: 151.2195};
        if(center) {
          mapCenter = center
        }
        this.map = new window.google.maps.Map(document.getElementById('map'), {
          center: mapCenter,
          zoom: 10,
          mapTypeId: 'roadmap'
        });

        this.updateResults(results);

        window.google.maps.event.addListener(this.map, 'click', (event) => {
            console.log(event);
            this.placeMarker(event.latLng)
        });
      }
      else this.props.onError()
    } else if(this.props.isScriptLoaded) {
      this.updateResults(results);
      if(center) {
        this.map.panTo(new window.google.maps.LatLng(center.lat, center.lng));
      } 
    }
  }

  updateResults = (results) => {
    if(results) {
      this.markers.forEach((marker) => {
        marker.setMap(null);
      });

      this.markers = [];

      results.forEach((result) => {
        let lat = parseFloat(result.coordinates.latitude);
        let lng = parseFloat(result.coordinates.longitude);
        var myLatlng = new window.google.maps.LatLng(lat, lng);
        var marker = new window.google.maps.Marker({
            position: myLatlng,
            map: this.map,
            title: result.name
        });
        this.markers.push(marker);
        window.google.maps.event.addListener(marker, 'click', function() {
          let win = window.open(result.url, '_blank');
          win.focus();
        });
      })
    }
  }

  placeMarker = (location) => {
    console.log(location.lat(), location.lng());
    new window.google.maps.Marker({
        position: location,
        map: this.map,
        title: "Dummy Title as of now"
    });
    this.props.onUpdateSearch([location.lat(), location.lng()], 'LOCATION')
  }

  render() {
    return (
      <div id='map' style={{'height':'100%'}}>
      </div>
    );
  }
}
export default scriptLoader(
    ['https://maps.googleapis.com/maps/api/js?key=AIzaSyDAQOhuvUriLPgDzVblnSSH7BUj-s2EMSw&libraries=places']
)(PlaceMapView)
